#include "bouton.h"
#include "rectangle.h"
#include "point.h"
#include <string.h>
#include <iostream>

using namespace std;


bouton::bouton() : r(), ch()
{
}

bouton::bouton(float px, float py, char* pch) : r(point(px,py), strlen(pch)+10,20), ch(pch)
{}

bouton::bouton(float px1, float py1, float px2, float py2, char* pch) : r(px1, py1, px2, py2), ch(pch)
{}

bouton::bouton(point p, float pl, float ph) : r(p, pl, ph), ch()
{}

bouton::bouton(point pp, float pl, float ph, char* pch) : r(pp,pl,ph), ch(pch)
{}

bouton::bouton(point pp1, point pp2, char* pch) : r(pp1, pp2), ch(pch)
{}

bouton::bouton(rectangle pr, char* pch) : r(pr), ch(pch)
{}

bouton::~bouton()
{
}

bouton::bouton(const bouton& s) : r(s.r), ch(s.ch)
{}

bouton& bouton::operator=(const bouton& s)
{
	ch = s.ch;
	r = s.r;
	return *this;
}

void bouton::affiche()
{
	cout << "objet bouton- " << "adresse= " << this << endl;
	r.affiche();
	cout << "longueur chaine = " << ch.length() << " et chaine= " << ch << endl;
}

void bouton::init(float px, float py, char* pch)
{
	string tmp(pch);
	ch = pch;
	r.init(px, py, px + ch.length() + 10, py + 60);
	//ch.append(pt);
}