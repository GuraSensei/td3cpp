#include "menubis.h"
#include <iostream>

using namespace std;

menubis::menubis(char* t[], int pn)
{
	n = pn;
	tab = new bouton * [n];
	for (int i = 0; i < n; i++)
		tab[i] = new bouton(10 * i, 5 * i, t[i]);
}

menubis::~menubis()
{
	for (int i = 0; i < n; i++)
		delete tab[i];
	delete tab;
}

menubis::menubis(const menubis& s)
{
	n = s.n;
	tab = new bouton * [n];
	for (int i = 0; i < n; i++)
		tab[i] = new bouton(* (s.tab[i]));
}

menubis& menubis::operator=(const menubis&s)
{
	if (this != &s)
	{
		for (int i = 0; i < n; i++)
			delete tab[i];
		delete tab;
		n = s.n;
		tab = new bouton * [n];
		for (int i = 0; i < n; i++)
			tab[i] = new bouton(*(tab[i]));
	}
	return *this;
}

void menubis::affiche()
{
	cout << "menu: " << endl;
	for (int i = 0; i < n; i++)
		tab[i]->affiche();
}

int menubis::selection(float px, float py)
/***********************************
* return -1 if no button selected  *
***********************************/
{
	for (int i = 0; i < n; i++)
		if (tab[i]->selection(px, py) == true) return i;
	return -1;
}