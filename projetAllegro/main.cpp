#include <iostream>
#include "bouton.h"
#include "menu.h"
#include "menubis.h"
#include "menuter.h"
#include "rectangle.h"
#include "point.h"

using namespace std;

int main(int argn, char* argv[])
{
	point p(10, 20); p.affiche();
	rectangle r1(p, 80, 40), r2(100, 100, 180, 140);
	r1.affiche(); r2.affiche();
	bouton btn(200, 200, (char*)"salut"); btn.affiche();
	bouton btn2(btn); btn2.affiche();
	char* t[] = { (char*)"btn1", (char*)"btn2", (char*)"btn3" };

	menu m1(t, 3); m1.affiche();
	menu m2(t, 2); m2.affiche();
	m2 = m1; m2.affiche();

	menu* m3 = new menu(t, 3); m3->affiche();
	delete m3;

	exit(0);
}