#include "rectangle.h"
#include "point.h"
#include <iostream>

using namespace std;

rectangle::rectangle() :p1(), p2()
{}
rectangle::rectangle(float px1, float py1, float px2, float py2) : p1(px1, py1), p2(px2, py2)
{}
rectangle::rectangle(point pp, float p1, float ph) : p1(pp), p2(pp.getx() + p1, pp.gety() + ph)
{}
rectangle::rectangle(point pp1, point pp2) : p1(pp1), p2(pp2)
{}
rectangle::~rectangle()
{}

rectangle::rectangle(const rectangle &s):p1(s.p1),p2(s.p2)
{
}

rectangle& rectangle::operator=(const rectangle& s)
{
	p1 = s.p1;
	p2 = s.p2;
	return *this;
}

void rectangle::init(float px1, float py1, float px2, float py2)
{
	p1.init(px1, py1); p2.init(px2, py2);
}

void rectangle::affiche()
{
	cout << "objet rectangle -" << "adresse = " << this << endl;
	p1.affiche(); p2.affiche();
	//rectfill(page, x1, y1, x2, y2, makecol(0, 255, 0));
}

bool rectangle::selection(float sx, float sy)
{
	if((sx >= p1.getx()) && (sx<=p2.getx()) && (sy>=p1.gety()) && (sy<=p2.gety())) return true;
	return false;
}