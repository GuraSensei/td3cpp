#pragma once
#include "bouton.h"

class menu
{
protected:
	bouton* tab;
	int n;
public:
	menu(char*[],int);
	~menu();
	menu(const menu&);
	menu& operator=(const menu&);
	void affiche();
	int selection(float, float);
};

